var dlApp = angular.module('dlApp', ['ui.router', 'treeControl', 'MessageCenterModule']);

dlApp.config(function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    var states = [
        {
            name: 'main',
            url: '/',
            template: 'Wrong state.'
        },
        {
            name: 'ds',
            url: '/ds/{dsId:int}',
            // 'differentialStudy'
            controller: function ($state, $stateParams, DifferentialStudyTask, messageCenterService) {
                var scope = this;

                scope.tabName = 'tree';

                scope.toggleTab = function (name) {
                    scope.tabName = scope.tabName !== name ? name : null;
                };

                scope.taskTree = DifferentialStudyTask.getTree();

                function getFlatTree(treeNodes, flatTree) {
                    flatTree = flatTree || [];
                    treeNodes.forEach(function (node) {
                        if (node.nodes) {
                            flatTree.push(node);
                            getFlatTree(node.nodes, flatTree);
                        }
                    });
                    return flatTree;
                }

                function getLeafs(treeNodes, leafs) {
                    leafs = leafs || [];
                    treeNodes.forEach(function (node) {
                        if (!node.nodes) {
                            leafs.push(node);
                        } else {
                            getLeafs(node.nodes, leafs);
                        }
                    });
                    return leafs;
                }

                function isSolved(node) {
                    return node.task.status === 'accepted';
                }

                scope.flatFilter = function (node) {
                    return !scope.onlyUnsolved || !isSolved(node);
                };

                scope.getLeafs = getLeafs;

                scope.expandedNodes = getFlatTree(scope.taskTree.nodes).filter(function (node) {
                    return !node.task;
                });

                console.log(scope.expandedNodes);

                scope.treeOptions = {
                    dirSelectable: false,
                    allowDeselect: false,
                    nodeChildren: 'nodes',
                    equality: function (o1, o2) {
                        return o1 === o2;
                    }
                };

                scope.onSelectNode = function (node) {
                    scope.currentNode = node;
                    scope.state.go('ds.node', {dsId: scope.params.dsId, nodeId: node.nodeId});
                };

                scope.state = $state;
                scope.params = $state.params;

                function selectCurrentNode() {
                    var candidates = getLeafs(scope.taskTree.nodes).filter(function (node) {
                        return node.nodeId === scope.params.nodeId;
                    });
                    if (candidates.length !== 0) {
                        scope.currentNode = candidates[0];
                    }
                }

                selectCurrentNode();

                // Bug IE
                var nodeToState = new WeakMap();

                function saveState(node, state) {
                    nodeToState.set(node, state);
                    return state;
                }

                function prepareTree(node) {
                    if (!node.nodes) {
                        return saveState(node, isSolved(node));
                    }
                    return saveState(node, node.nodes
                        .map(function (node) {
                            return saveState(node, prepareTree(node));
                        })
                        .every(function (solved) {
                            return solved;
                        })
                    );
                }

                // TODO get attention performance!
                scope.treeFilter = function (node) {
                    if (!scope.onlyUnsolved) {
                        return true;
                    }

                    // TODO get attention on includes (Bug IE)
                    if (scope.taskTree.nodes.includes(node)) {
                        //messageCenterService.add('success', 'Bye bye in 3s!', { timeout: 10000 });
                        prepareTree(node);
                    }

                    return !nodeToState.get(node);
                };

                messageCenterService.add('info', 'Решение успешно отправлено на проверку', { timeout: 1000000 });

                // -----------------------------------------------------

                // make all nodes expanded
            },
            controllerAs: 'ds',
            templateUrl: 'differential-study.template.html'
            // template: '<div>zdarova</div><ui-view></ui-view>'
            // ,
            // resolve: {
            //   people: function(PeopleService) {
            //     return PeopleService.getAllPeople();
            //   }
            // }
        },
        {
            name: 'ds.node',
            url: '/node/{nodeId:int}',
            // 'differentialStudyNode'
            controller: function ($stateParams, DifferentialStudyTask) {
                this.params = $stateParams;
            },
            controllerAs: 'dsNode',
            // template: '<div>{{ds.params}} {{dsNode.params}}</div>'
            templateUrl: 'differential-study-task.template.html'
            // ,
            // resolve: {
            //   person: function(people, $stateParams) {
            //     return people.find(function(person) {
            //       return person.id === $stateParams.personId;
            //     });
            //   }
            // }
        }
    ];

    states.forEach(function (state) {
        $stateProvider.state(state);
    });
});

dlApp.factory('DifferentialStudyTask', function () {
    function getTree(dsId) {
        return {
            nodes: [
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "First Node",
                    nodeId: 407268,
                    num: 8343242,
                    nodes: [
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                        {
                            title: "Second Node",
                            nodeId: 402203,
                            num: 8343244,
                            task: {
                                title: "Task 1",
                                taskId: 324367,
                                status: 'clean'
                            }
                        },
                        {
                            title: "Third Node",
                            nodeId: 407277,
                            num: 8343249,
                            task: {
                                title: "Task 2",
                                taskId: 324367,
                                status: 'attempted'
                            }
                        },
                        {
                            title: "Fourth node",
                            nodeId: 407278,
                            num: 8343249,
                            nodes: [
                                {
                                    title: "Node",
                                    nodeId: 407203,
                                    num: 8343250,
                                    task: {
                                        title: "Task 3",
                                        taskId: 324361,
                                        status: 'accepted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407213,
                                    num: 8343255,
                                    task: {
                                        title: "Task 4",
                                        taskId: 324371,
                                        status: 'attempted'
                                    }
                                },
                                {
                                    title: "Node",
                                    nodeId: 407223,
                                    num: 8343260,
                                    task: {
                                        title: "Task 5",
                                        taskId: 324362,
                                        status: 'accepted'
                                    }
                                }
                            ]
                        },
                    ]
                }
            ],
            meta: {
                courseId: 620
            }
        }
    }

    function getTask(courseId, nodeId) {
        return {
            title: "Task",
            dir: ["First Node", "Second Node"],
            data: ""
        }
    }

    return {
        getTree: getTree,
        getTask: getTask
    }
});